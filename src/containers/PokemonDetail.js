import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GetPokemon } from '../actions/pokemonAction';
import AddMyPokemon from '../components/AddMyPokemon';
import lodash from 'lodash'

const PokemonDetail = (props) => {
  const pokemonFromProps = {...props.location.state};
  const pokemonName = props.match.params.pokemon
  const dispatch = useDispatch();
  const pokemonDetailState = useSelector(state => state.PokemonDetail)

  React.useEffect(() => {
    dispatch(GetPokemon(pokemonName))
  }, [])

  const showData = () => {
    if(!lodash.isEmpty(pokemonDetailState.data[pokemonName])){
      const pokemonDetail = pokemonDetailState.data[pokemonName];
      return (
        <div className="pokemon-detail-container">
          <div className="pokemon-item">
            <h1>Sprites</h1>
            <div className="image-container">
              <img src={pokemonDetail.sprites.front_default} alt={`${pokemonDetail.species.name}`}></img>
            </div>
            <div className="mb-1">
              <h3>Moves</h3>
            </div>
            <div className="skill-container border border-darken-2 rounded">
              {pokemonDetail.moves.map((item) => {
                return (
                  <div className="border border-darken-1 m-1 p-1 bg-warning text-dark rounded">{item.move.name}</div>
                )
              })}  
            </div>
            <div className="mb-1">
              <h3>Types</h3>
            </div>
            <div className="skill-container border border-darken-2 rounded">
              {pokemonDetail.types.map((item) => {
                return (
                  <div className="border border-darken-1 m-1 p-1 bg-success text-light rounded">{item.type.name}</div>
                )
              })}  
            </div>          
            {/* <img src={pokemonDetail.sprites.back_default} alt={`${pokemonDetail.species.name}`}></img>
            <img src={pokemonDetail.sprites.front_shiny} alt={`${pokemonDetail.species.name}`}></img>
            <img src={pokemonDetail.sprites.back_shiny} alt={`${pokemonDetail.species.name}`}></img> */}
          </div>
          <div className="pokemon-item">
            <h1>Stats</h1>
            {pokemonDetail.stats.map((itemStat) => {
              return (
                <div>
                  <p>{itemStat.stat.name} : {itemStat.base_stat}</p>
                </div>
              )
            })}
          </div>
          <div className="pokemon-item">
           <h1>Abilities</h1>
           {pokemonDetail.abilities.map((el) => {
             return (
               <div>
                 <p>{el.ability.name}</p>
               </div>
             )
           })}
          </div>
        </div>
      )
    }

    if(pokemonDetailState.loading){
      return <p>loading...</p>
    }

    if(pokemonDetailState.message !== ""){
      return <p>{pokemonDetailState.message}</p>
    }

    return <p>error occured</p>
  }

  const [buttonToggle, setButtonToggle] = useState(false);

  return (
    <div className="poke-wrapper">
      <h1>{pokemonName}</h1>
      <button className="mb-4 btn-success rounded btn-lg" data-bs-toggle={'modal'} data-bs-target={"#nickname-modal"} onClick={() => setButtonToggle(!buttonToggle)}>Catch Pokemon</button>
      <AddMyPokemon {...pokemonFromProps}></AddMyPokemon>
      {/* {buttonToggle && <AddMyPokemon {...pokemonFromProps}></AddMyPokemon>} */}
      {showData()}</div>
  )
}

export default PokemonDetail;