import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import lodash from 'lodash' 
import { GetPokemonList } from '../actions/pokemonAction'
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

const PokemonList = (props) => {
  const [search, setSearch] = useState("");

  const dispatch = useDispatch();
  const pokemonList = useSelector(state => state.PokemonList) // ketika state.PokemonList berubah maka const pokemonList juga ikut berubah

  const fetchData = (page = 1) => {
    dispatch(GetPokemonList(page))
  }

  React.useEffect(() => {
    fetchData(1);
  }, [])

  const showData = () => {  

    if(pokemonList.loading === true){
      return <p>is loading....</p>
    }

    if(!lodash.isEmpty(pokemonList.data)){
      return (
        <div className="pokemon-list-container">
          {
            pokemonList.data.map(el => {
              return <div key={el.url+el.name} className="pokemon-element">
                <p>{el.name}</p>
                <Link to={{
                  pathname: `/pokemon-detail/${el.name}`,
                  state: {
                    name: el.name,
                    url: el.url
                  }
                }}>View</Link>
              </div>
            })
          }
        </div>
      )
    }

    if(pokemonList.message !== ''){
      return <p>{pokemonList.message}</p>
    }

    return <p>Unable to get data</p>
  }


  return (
    <div>
      <div className="search-wrapper">
        <p className="me-2">Search : </p>
        <input type="text" onChange={e => setSearch(e.target.value)}></input>
        <button className="btn btn-info ms-2 rounded" onClick={() => props.history.push(`/pokemon-detail/${search}`)}>Do search</button>
      </div>
      {showData()}
      {!lodash.isEmpty(pokemonList.data) && (
        <ReactPaginate 
          pageCount={Math.ceil(pokemonList.count / 15)}
          pageRangeDisplayed={2}
          marginPagesDisplayed={1}
          onPageChange={(data) => fetchData(data.selected + 1)}
          containerClassName="pokemon-list-pagination"
        ></ReactPaginate>
      )}
    </div>
  )
}

export default PokemonList;