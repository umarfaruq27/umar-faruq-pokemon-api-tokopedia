import React, { useState } from 'react';
import {useSelector, useDispatch } from 'react-redux';
import lodash from 'lodash' 
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

const MyPokemonList = (props) => {
  const dispatch = useDispatch();
  const [search, setSearch] = useState("");
  const pokemonList = useSelector(state => state.MyPokemonList)
  const [pagingList, setPagingList] = useState([]);

  const fetchData = (page) => {
    const perPage = 15;
    const offset = (page * perPage) - perPage;
    const copyState = [...pokemonList.data];
    var result = copyState.slice(offset, (offset+perPage));
    setPagingList([...result]);
  }

  const removeMyPokemon = (nick) => {
    dispatch({
      type: 'REMOVE_MY_POKEMON_LIST',
      payload: {
        nickname: nick
      }
    })
    fetchData(1);
  }

  React.useEffect(() => {
    fetchData(1);
  }, [])

  const showData = () => {  
      return (
        <div className="pokemon-list-container table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Name</th>
                <th scope="col">Nickname</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
          {
            pagingList.map((item, index) => {
              return (
                <tr>
                  <td>{index+1}</td>
                  <td>{item.name}</td>
                  <td>{item.nickname}</td>
                  <td>
                    <button className="btn btn-info rounded me-2">                      
                      <Link className="text-decoration-none text-light" to={{
                        pathname: `/pokemon-detail/${item.name}`,
                        state: {
                          name: item.name,
                          url: item.url
                        }
                      }}>View</Link>
                    </button>
                    <button className="btn btn-danger rounded" onClick={() => removeMyPokemon(item.nickname)}>Remove</button>
                  </td>
                </tr>
              )
            })
          }
            </tbody>
          </table>
        </div>
      )
  }


  return (
    <div>
      <div className="search-wrapper">
        <p>Search :</p>
        <input type="text" onChange={e => setSearch(e.target.value)}></input>
        <button className="btn btn-info ms-2 rounded" onClick={() => props.history.push(`/pokemon-detail/${search}`)}>Do search</button>
      </div>
      {showData()}
      {!lodash.isEmpty(pagingList.data) && (
        <ReactPaginate 
          pageCount={Math.ceil(pagingList.length / 15)}
          pageRangeDisplayed={2}
          marginPagesDisplayed={1}
          onPageChange={(data) => fetchData(data.selected + 1)}
          containerClassName="pokemon-list-pagination"
        ></ReactPaginate>
      )}
    </div>
  )
}

export default MyPokemonList;