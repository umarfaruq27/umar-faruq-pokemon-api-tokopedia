import React from 'react';
import './App.css';
import {Link} from 'react-router-dom'; // berfungsi untuk menuju route tertentu

const navStyle = {
  color: "white",
  textDecoration: "none"
}
function Nav(){
  return(
    <nav>
      <Link style={navStyle} to="/">
        <h3>Logo</h3>
      </Link>
      <ul className="nav-links">
      </ul>
    </nav>
  )
}

export default Nav;