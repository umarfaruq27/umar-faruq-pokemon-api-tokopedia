/* eslint-disable react/jsx-no-comment-textnodes */
import React from 'react';
import { Switch, Route, NavLink, Redirect } from 'react-router-dom';
import './App.css';
import PokemonList from './containers/PokemonList';
import PokemonDetail from './containers/PokemonDetail';
import MyPokemonList from './containers/MyPokemonList'

 /* NavLink hanya lah sebuah anchor tag biasa. Fungsi nya adalah jika kita berada di route yang ada di nilai 'to' tersebut, maka akan memberikan toggle class active pada tag anchor tersebut */
/* Switch berfungsi untuk menghindari terjadi nya penumpukan render component. Misal kita ingin 
 ke /about, maka yg terjadi jika tidak menggunakan switch adalah kita akan mendapatkan dua komponen yg terender yaitu / (Home) dan /about (About) */

/* atribute exact berfungsi untuk menyatakan bahwa hanya komponen yang memiliki path "/" saja yang akan di render*/
 function App() {
  return (      
    <div className="App">
      <nav>
        <NavLink to={"/"}>Search</NavLink>
        <NavLink to={"/my-pokemon-list"}>My Pokemon List</NavLink>
      </nav>
      <Switch>
        <Route path="/" exact component={PokemonList}></Route> 
        <Route path="/pokemon-detail/:pokemon" exact component={PokemonDetail}></Route>
        <Route path="/my-pokemon-list" exact component={MyPokemonList}></Route>
        <Redirect to={"/"}></Redirect>
      </Switch>
    </div>
  );
}

export default App;
