import { useState } from "react";
import { useDispatch } from "react-redux";

function AddMyPokemon(props) {
  const dispatch = useDispatch();

  const [nickname, setNickname] = useState('');

  const handleChangeInput = (e) => {
    e.persist();
    setNickname(e.target.value);
  } 

  const addToMyPokemonList = () => {
    dispatch({
      type: 'ADD_MY_POKEMON_LIST',
      payload: {
        name: props.name,
        nickname: nickname,
        url: props.url
      }
    })

    setNickname(''); // reset lagi
  }
  return ( 
    <div id="nickname-modal" className="modal" tabIndex="-1">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">You want to catch {props.name}</h5>
            <button type="button" className="btn-close" data-bs-dismiss={"modal"} aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <input onChange={(e) => handleChangeInput(e)} value={nickname} placeholder={"Please input nickname"}></input>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss={"modal"}>Close</button>
            <button className="btn btn-primary rounded" onClick={addToMyPokemonList} data-bs-dismiss={'modal'} type="button">Save Pokemon</button>
          </div>
        </div>
      </div>
    </div>  
  )
}

export default AddMyPokemon;