import axios from 'axios';

export const GetPokemonList = (page) => async dispatch => {
  try {
    dispatch({
      type: "POKEMON_LIST_LOADING"
    })
    const perPage = 15
    const offset = (perPage * page) - perPage;
    const respon = await axios.get(`https://pokeapi.co/api/v2/pokemon?limit=15&offset=${offset}`)
    dispatch({
      type: 'POKEMON_LIST_SUCCESS',
      payload: respon.data
    })
  } catch (error) {
    dispatch({
      type: 'POKEMON_LIST_FAIL'
    })
  }
}

export const GetPokemon = (pokemonName) => async dispatch => {
  try {
    dispatch({
      type: 'POKEMON_MULTIPLE_LOADING'
    })

    const respon = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`);

    dispatch({
      type: 'POKEMON_MULTIPLE_SUCCESS',
      payload: respon.data,
      pokemonName: pokemonName
    })
  } catch (error) {
    dispatch({
      type: 'POKEMON_MULTIPLE_FAIL'
    })
  }
}
