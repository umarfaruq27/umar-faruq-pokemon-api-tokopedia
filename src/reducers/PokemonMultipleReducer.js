const initialState = {
  loading: false,
  data: {},
  message: ''
}

const PokemonMultipleReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'POKEMON_MULTIPLE_LOADING':
      return {
        ...state,
        loading: true,
        message: ""
      }
    case 'POKEMON_MULTIPLE_SUCCESS':
      return {
        ...state,
        loading: false,
        data: {
          ...state.data,
          [action.pokemonName]: action.payload
        },
        message: ""
      }
    case 'POKEMON_MULTIPLE_FAIL':
      return {
        ...state,
        loading: false,
        message: "Failed to fetch data from the server"
      }
  
    default:
      return state;
  }
}

export default PokemonMultipleReducer;