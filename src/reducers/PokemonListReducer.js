const initialState = {
  loading: false,
  message: '',
  data: [],
  count: 0
}

const PokemonListReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'POKEMON_LIST_LOADING':
      return {
        ...state, 
        loading: true,
        message: ""
      }
    case 'POKEMON_LIST_SUCCESS':
      return {
        ...state, 
        loading: false,
        message: 'Success to fetch data from server',
        data: action.payload.results,
        count: action.payload.count
      }
    case 'POKEMON_LIST_FAIL':
      return {
        ...state,
        loading: false,
        message: 'Failed to fetch data from server'
      }
  
    default:
      return state;
  }
}

export default PokemonListReducer;