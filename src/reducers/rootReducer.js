import { combineReducers } from "redux";
import PokemonListReducer from './PokemonListReducer';
import PokemonMultipleReducer from './PokemonMultipleReducer';
import MyPokemonListReducer from './MyPokemonListReducer';

const rootReducer = combineReducers({
  PokemonList: PokemonListReducer,
  PokemonDetail: PokemonMultipleReducer,
  MyPokemonList: MyPokemonListReducer
});


export default rootReducer;