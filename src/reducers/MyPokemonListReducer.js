const initialState = {
  data: [{
    name: '',
    nickname: '',
    url: ''
  }],
  message: '',
  statusSuccess: false
}

const MyPokemonListReducer = (state = initialState, action) => {
  const {type, payload} = action;
  
  switch (type) {
    case 'ADD_MY_POKEMON_LIST':
      
      var copyState = {...state}
      if(state.data.length > 0){
        var statusExisting = copyState.data.some((el) => el.nickname === payload.nickname);
        if(!statusExisting){          
          copyState.data.push(payload);
          copyState.statusSuccess = true;
          copyState.message = 'Item added successfully'
        } else {
          copyState.statusSuccess = false;
          copyState.message = 'Item failed to add because Nickname already exist'
        }
      } else {
        copyState.data.push(payload)        
        copyState.statusSuccess = true;
        copyState.message = 'Item added successfully'
      }
      return {
        ...state,
        data: copyState.data,
        message: copyState.message,
        statusSuccess: copyState.statusSuccess
      }; // lakukan return seperti ini agar redux-persist worked well
    
    case "REMOVE_MY_POKEMON_LIST":
      var copyStateRemove = {...state};
      var indexDeleted = copyStateRemove.data.findIndex((item) => item.nickname === payload.nickname);
      copyStateRemove.data.splice(indexDeleted,1);
      return {
        ...copyStateRemove
      }
  
    default:
      return state;
  }
}

export default MyPokemonListReducer;